package namespaceinpath

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCustomResponseWriter_WriteHeader(t *testing.T) {
	pagesDomain := "example.com"
	authRedirectURI := "http://example.com/projects/auth"

	cases := []struct {
		name             string
		locationHeader   string
		expectedLocation string
		statusCode       int
	}{
		{
			name:             "Valid location header conversion",
			locationHeader:   "https://namespace.example.com/a-path",
			expectedLocation: "https://example.com/namespace/a-path",
			statusCode:       http.StatusFound,
		},
		{
			name:             "Location header is an auth URL",
			locationHeader:   authRedirectURI,
			expectedLocation: authRedirectURI,
			statusCode:       http.StatusFound,
		},
		{
			name:             "Location header is empty",
			locationHeader:   "",
			expectedLocation: "",
			statusCode:       http.StatusFound,
		},
		{
			name:             "Custom domain location header conversion",
			locationHeader:   "https://namespace.another.com/a-path",
			expectedLocation: "https://namespace.another.com/a-path",
			statusCode:       http.StatusFound,
		},
		{
			name:             "Valid location header with no schema conversion",
			locationHeader:   "//namespace.example.com/a-path",
			expectedLocation: "//example.com/namespace/a-path",
			statusCode:       http.StatusFound,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			recorder := httptest.NewRecorder()
			writer := newResponseWriter(recorder, pagesDomain, authRedirectURI)

			// Set the Location header
			if c.locationHeader != "" {
				writer.Header().Set("Location", c.locationHeader)
			}

			// Write the header with the provided status code
			writer.WriteHeader(c.statusCode)

			// Assert the Location header
			require.Equal(t, c.expectedLocation, writer.Header().Get("Location"))
		})
	}
}

func TestCustomResponseWriter_IsAuthURL(t *testing.T) {
	pagesDomain := "example.com"
	authRedirectURI := "http://example.com/projects/auth"

	cases := []struct {
		rawURL         string
		expectedResult bool
	}{
		{
			rawURL:         authRedirectURI,
			expectedResult: true,
		},
		{
			rawURL:         "http://example.com/namespace/auth",
			expectedResult: true,
		},
		{
			rawURL:         "http://example.com/not-auth",
			expectedResult: false,
		},
		{
			rawURL:         "http://example.com/",
			expectedResult: false,
		},
		{
			rawURL:         "http://namespace.example.com/auth",
			expectedResult: true,
		},
		{
			rawURL:         "http://namespace.example.com/not-auth",
			expectedResult: false,
		},
		{
			rawURL:         "http://another.com/auth",
			expectedResult: false,
		},
		{
			rawURL:         "http://another.com/namespace/auth",
			expectedResult: false,
		},
	}

	for _, c := range cases {
		t.Run(c.rawURL, func(t *testing.T) {
			writer := newResponseWriter(httptest.NewRecorder(), pagesDomain, authRedirectURI)

			parsedURL, err := url.Parse(c.rawURL)
			require.NoError(t, err)

			result := writer.isAuthURL(parsedURL)
			require.Equal(t, c.expectedResult, result)
		})
	}
}
